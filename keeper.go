package main

import (
"fmt"
"code.google.com/p/go-sqlite/go1/sqlite3"
)

func keeper(dbName string){
	fmt.Println("DB started")
	
	conn, err := sqlite3.Open(dbName)
	if err != nil {
		fmt.Println("Unable to open the database: %s", err)
		panic("Can't open DB")
	}

	defer conn.Close()
	err = conn.Exec("CREATE TABLE IF NOT EXISTS rentals(hash TEXT PRIMARY KEY, link TEXT, desc TEXT, date STRING, dateFound INTEGER);")
	if err != nil {
		fmt.Println("Cannot execute: %s", err)	
        }
}