package main

import (
"bytes"
"code.google.com/p/go.net/html"
"code.google.com/p/go-sqlite/go1/sqlite3"
"fmt"
"os/exec"
"crypto/sha1"
"encoding/base64"
"time"
"math/rand"
)


type RentItem struct {
	Hash, Link, Desc, Date string
	TimeFound int64
}

func saveNode(n *html.Node, conn *sqlite3.Conn) {
	if(n.Parent.Data == "p"){
	fmt.Println(n.Data)
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		saveNode(c, conn)
	}
}

func getText(n *html.Node) string{
	result := ""
	for c:= n.FirstChild; c != nil; c = c.NextSibling{
		if c.Data == "p"{
			for c1 := c.FirstChild; c1 != nil; c1 = c1.NextSibling{
				result += c1.Data
			}
		}
	}
	return result
}

func getLink(n *html.Node) string{
	for c:= n.FirstChild; c != nil; c = c.NextSibling{
	        if c.Data == "a"{
                        for _, a := range c.Attr {
                      		if a.Key == "href" {
				//	fmt.Println("Link found", a.Val)
                                        return a.Val
				}
			}
		}
	}
return ""	
}

func (rentItem RentItem) save(conn * sqlite3.Conn) bool{
	hasher := sha1.New()
//	fmt.Printf("bytes from %v\n", rentItem.Desc)
	hasher.Write([]byte(rentItem.Desc))
	rentItem.Hash = base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	rentItem.TimeFound = time.Now().Unix()
//	fmt.Println("Hash is %v", sha)

//	fmt.Println("Saving %+v", rentItem)
	args := sqlite3.NamedArgs{"$hash" : rentItem.Hash, "$link" : rentItem.Link, "$desc" : rentItem.Desc, "$date" : rentItem.Date, "$dateFound" : rentItem.TimeFound }
	err := conn.Exec("INSERT INTO rentals VALUES($hash, $link, $desc, $date, $dateFound)", args)
	if err != nil{
		//fmt.Println("Error in insert %v",err)
		return false
	}
	return true
}

func parse(n *html.Node, conn * sqlite3.Conn, rentItem * RentItem) (int, int) {
	foundNew := 0
	found := 0
	if n.Type == html.ElementNode && n.Data == "div" {
        for _, a := range n.Attr {
			if a.Key == "class"{
				switch a.Val {
				case "rent-item-info__date pull-right":
					rentItem.Date = n.FirstChild.Data
					rentItem.Desc = ""
				//	fmt.Printf("Found date %v\n",rentItem.Date)
				case "rent-item__text":
					rentItem.Desc = getText(n)
				//	fmt.Printf("Found text %v\n", rentItem.Desc)
				case "rent-item__link":
					rentItem.Link = getLink(n)
				//	fmt.Printf("Collected %+v\n", rentItem)
					found++
					if rentItem.save(conn) {
						foundNew++
					}
				}
			}
		}
        }
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		var _found, _foundNew = parse(c, conn, rentItem)
		found += _found
		foundNew += _foundNew
	}
	return found, foundNew
}

func thief(dbName string){
	r := rand.New(rand.NewSource(9))
	var sleepLevel = 0
	fmt.Println("Thief started")
	for{		
		conn, err := sqlite3.Open(dbName)
		if err != nil {
			fmt.Println("can't open db: %s", err)
			panic("Can't open db")
		}	
		defer conn.Close()

		out, err := exec.Command("curl", "friendrent.ru/", "-b cookie1").Output()
		if err != nil {
			fmt.Println("error occured")
			fmt.Printf("%s", err)
		}

		//fmt.Printf("%s", out)
		doc, err := html.Parse(bytes.NewReader(out))

		if err != nil {
			fmt.Println("error occured")
			fmt.Printf("%s", err)
		}

		
		var found, foundNew = parse(doc, conn, new(RentItem))
		
		fmt.Printf("Found %v items %v new | ",found, foundNew)
		var sleepDuration time.Duration
		switch(foundNew){
		case 0:
			if sleepLevel < 100  {
				sleepLevel++
			}else{
				sleepLevel = 0
			}
			sleepDuration = time.Duration(5 + (1 + r.Intn(5))*sleepLevel) * time.Second
		case 1:
			sleepLevel /= 2
			if sleepLevel < 0{
				sleepLevel = 0
			}
			sleepDuration = time.Duration(5 + r.Intn(10)) * time.Second
		case 2,3:
			sleepLevel = 0
			sleepDuration = time.Duration(5 + r.Intn(5)) * time.Second			
		}
		fmt.Printf("sleep for %v, current sleep level is %v\n",sleepDuration,sleepLevel)
		time.Sleep(sleepDuration)
	}

}